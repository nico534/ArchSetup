bash 0-preinstall.sh
cp -r ~/ArchSetup /mnt/root/
arch-chroot /mnt /root/ArchSetup/1-setupsystem.sh
source ~/ArchSetup/install.conf
cp -r ~/ArchSetup /mnt/home/$USERNAME/
arch-chroot /mnt chown -R $USERNAME:$USERNAME /home/$USERNAME
arch-chroot /mnt /usr/bin/runuser -u $USERNAME -- /home/$USERNAME/ArchSetup/2-user.sh
rm -r /mnt/root/ArchSetup
rm -r /mnt/home/$USERNAME/ArchSetup
umount /mnt/boot/EFI
umount /mnt
echo "Installation finished, you can reboot now by typing \"reboot\"."
