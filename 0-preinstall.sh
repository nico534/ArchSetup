#!/bin/bash
echo "-------------------------------------------------"
echo "Setting up mirrors for optimal download          "
echo "-------------------------------------------------"
iso=$(curl -4 ifconfig.co/country-iso)
timedatectl set-ntp true
pacman -S --noconfirm pacman-contrib terminus-font
setfont ter-v22b
sed -i 's/^#Para/Para/' /etc/pacman.conf
pacman -S --noconfirm reflector rsync
mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup

reflector -a 48 -c "$iso" -f 5 -l 20 --sort rate --save /etc/pacman.d/mirrorlist
pacman -S --noconfirm gptfdisk

echo "-------------------------------------------------"
echo "-------select your disk to format----------------"
echo "-------------------------------------------------"
lsblk
echo "Please enter disk to work on: (example /dev/sda)"
read DISK
echo "THIS WILL FORMAT AND DELETE ALL DATA ON THE DISK"
read -p "are you sure you want to continue (Y/N):" formatdisk
case $formatdisk in

y|Y|yes|Yes|YES)
echo "--------------------------------------"
echo -e "\nFormatting disk...\n$HR"
echo "--------------------------------------"

# disk prep
sgdisk -Z "${DISK}" # zap all on disk
#dd if=/dev/zero of=${DISK} bs=1M count=200 conv=fdatasync status=progress
sgdisk -a 2048 -o "${DISK}" # new gpt disk 2048 alignment

# create partitions
sgdisk -n 1:0:+550M "${DISK}" # partition 1 (UEFI SYS), default start block, 512MB
sgdisk -n 2:0:0     "${DISK}" # partition 2 (Root), default start, remaining

# set partition types
sgdisk -t 1:ef00 "${DISK}"
sgdisk -t 2:8300 "${DISK}"

# label partitions
sgdisk -c 1:"UEFISYS" "${DISK}"
sgdisk -c 2:"ROOT" "${DISK}"

# make filesystems
echo -e "\nCreating Filesystems...\n$HR"
if [[ "${DISK}" =~ "nvme" ]]; then
mkfs.vfat -F32 -n "UEFISYS" "${DISK}p1"
mkfs.ext4 -L "ROOT" "${DISK}p2"
else
mkfs.vfat -F32 -n "UEFISYS" "${DISK}1"
mkfs.ext4 -L "ROOT" "${DISK}2"
fi
;;
esac

mount -L ROOT /mnt
mkdir /mnt/boot
mkdir /mnt/boot/EFI
mount -L UEFISYS /mnt/boot/EFI

pacstrap /mnt base base-devel linux linux-firmware
genfstab -U /mnt >> /mnt/etc/fstab
cp /etc/pacman.d/mirrorlist /mnt/etc/pacman.d/mirrorlist
