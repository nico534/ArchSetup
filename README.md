# ArchSetup Script

This README contains the steps I do to install and configure a Arch Linux installation containing the xmonad window-manager, a few support packages (network, bluetooth, audio, printers, etc.), along with all my preferred applications, configurations and utilities. The shell scripts in this repo allow the entire process to be automated.

<img src="https://gitlab.com/nico534/ArchSetup/-/raw/main/Screenshot/Image_1.png" />

This script / window manager may need some tinkering and linux / shell / git / xmonad knowledge :)

---
## Create Arch ISO or Use Image

Download ArchISO from <https://archlinux.org/download/> and put on a USB drive with Ventoy or Etcher

## Boot Arch ISO

Change Keyboard-Layout to german use
```
loadkeys de-latin1-nodeadkeys
```
`y` is `z` and `-` is `ß` on a german keyboard.

## Install Arch

Clone ArchSetup from GitLab

```
pacman -Sy git
git clone https://gitlab.com/nico534/ArchSetup.git
cd ArchSetup
```

Update settings in `install.conf` to your username, hostname, language.

Defaults:
```
USERNAME=pc
HOSTNAME=pc
KEYBOARD_LAYOUT=de-latin1-nodeadkeys
LANGUAGE=en_DK.UTF-8
```

Runn installarch.sh
```
./installarch.sh
```

You need to enter root password, user password and accept a few things.

## Troubleshooting

__[Arch Linux Installation Guide](https://github.com/rickellis/Arch-Linux-Install-Guide)__

## Credits

- Original packages script was a post install cleanup script called ArchMatic located here: https://github.com/rickellis/ArchMatic
- My config-files are mostly from <https://gitlab.com/dwt1/dotfiles> (Great YouTube / Odyssee Channel for linux-stuff <https://odysee.com/@DistroTube:2>)
