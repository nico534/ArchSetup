#!/bin/bash
# Install xorg-system, grub... and setup
source /root/ArchSetup/install.conf

# Set locale-timezone to Europe/Berlin (Germany):
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# Set Hardware-clock
hwclock --systohc

# generate locale de_DE.UTF-8 and en_DK.UTF-8 for time
sed -i "s/^#$LANGUAGE/$LANGUAGE/" /etc/locale.gen
#sed -i 's/^#en_DK.UTF-8 UTF-8/en_DK.UTF-8 UTF-8/' /etc/locale.gen
locale-gen

# Change Keyboard-Layout 
localectl --no-ask-password set-keymap "$KEYBOARD_LAYOUT"
echo "KEYMAP=$KEYBOARD_LAYOUT" >> /etc/vconsole.conf

echo "LANG=$LANGUAGE" >> /etc/locale.conf

# Set hostname
echo $HOSTNAME > /etc/hostname

echo "127.0.0.1    localhost" >> /etc/hosts
echo "::1          localhost" >> /etc/hosts
echo "127.0.1.1    $HOSTNAME.localdomain $HOSTNAME" >> /etc/hosts

# Enable parallel pacman download
sed -i 's/^#Para/Para/' /etc/pacman.conf

#
# Create and setup user
#
echo "Enter root user password"
passwd

useradd -m $USERNAME
echo "Enter $USERNAME password"
passwd $USERNAME

usermod -aG wheel,video,audio,optical,storage $USERNAME

pacman -S archlinux-keyring --noconfirm

pacman -S sudo --noconfirm
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

#
# Install grub (efi-system)
#
pacman -S --noconfirm grub efibootmgr dosfstools os-prober mtools

grub-install --target=x86_64-efi --bootloader-id=grub_uefi --recheck
grub-mkconfig -o /boot/grub/grub.cfg


# Check processor type and install microcode
proc_type=$(lscpu | awk '/Vendor ID:/ {print $3}')
case "$proc_type" in
	GenuineIntel)
		print "Installing Intel microcode"
		pacman -S --noconfirm intel-ucode
		;;
	AuthenticAMD)
		print "Installing AMD microcode"
		pacman -S --noconfirm amd-ucode
		;;
esac

# Check and install graphics drivers
if lspci | grep -E "NVIDIA|GeForce"; then
	print "Install and setup NVIDIA graphics drivers"
	pacman -S nvidia --noconfirm --needed
	nvidia-xconfig
elif lspci | grep -E "Radeon"; then
	print "Install AMD graphics drivers"
	pacman -S xf86-video-amdgpu --noconfirm --needed
elif lspci | grep -E "Integrated Graphics Controller"; then
	print "Install integrated intel graphics drivers"
	pacman -S libva-intel-driver libvdpau-va-gl lib32-volkan-intel vulkan-intel libva-intel-driver libva-utils --needed --noconfirm
fi

echo "Update and install applications"
# Install packages
pacman -Syyu
# prevent iptables-nft conflicting package question
yes | pacman -S --needed iptables-nft

pacman -S --needed --noconfirm $(comm -12 <(pacman -Slq | sort) <(sort "$HOME/ArchSetup/packages/system.txt"))
pacman -S --needed --noconfirm $(comm -12 <(pacman -Slq | sort) <(sort "$HOME/ArchSetup/packages/applications.txt"))


TO_ENABLE_LIST=$(cat "$HOME/ArchSetup/packages/systemd-enable.txt")
for TO_ENABLE in $TO_ENABLE_LIST
do
	systemctl enable "$TO_ENABLE"
done

python -m ensurepip --upgrade

# Set qt5ct env-var
echo "QT_QPA_PLATFORMTHEME=qt5ct" | tee -a /etc/environment

cp -r $HOME/.dotfiles/wallpaper /usr/share/wallpaper

#
# Lightdm settings
#
sed -i 's/^#theme-name=/theme-name=Adapta-Nokto-Maia/' /etc/lightdm/lightdm-gtk-greeter.conf
sed -i 's/^#icon-theme-name=/icon-theme-name=Papirus-Dark-Maia/' /etc/lightdm/lightdm-gtk-greeter.conf
 sed -i 's:^#background=:background=/usr/share/wallpaper/wallpaper.jpg:' /etc/lightdm/lightdm-gtk-greeter.conf


#
# xorg settings
#
mkdir -p /etc/X11/xorg.conf.d
cp $HOME/ArchSetup/X11/* /etc/X11/xorg.conf.d/
