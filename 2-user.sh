#!/bin/bash

# if nvidia, run nvidia-xconfig
if lspci | grep -E "NVIDIA|GeForce"; then
	nvidia-xconfig
fi

echo "CLONING: YAY"
cd ~
git clone "https://aur.archlinux.org/yay.git"
cd ~/yay
makepkg -si --noconfirm

yay -S --needed --removemake --cleanafter --noconfirm $(comm -12 <(yay -Slq | sort) <(sort ~/ArchSetup/packages/yay.txt))

git clone https://gitlab.com/nico534/dotfiles.git $HOME/.dotfiles

cd $HOME/.dotfiles
git checkout -b system-branch

# Remove default xmonad-folder
rm -r $HOME/.xmonad

echo -e "\nCreate symlinks"
$HOME/.dotfiles/configs/scripts/createSymlinks.sh


#
# Theaming
# 
echo -e "\nSet dark theme"
$HOME/.config/scripts/changeTheme.sh 0


cp -r $HOME/.dotfiles/wallpaper ~/Pictures/wallpaper

# Set Firefox as default Browser
xdg-settings set default-web-browser firefox.desktop

# install vim-plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

python -m pip install --upgrade pip
pip3 install --user neovim

# Install nvim-plugs
nvim --headless +PlugInstall +qall

# install starship shell-prompt
sh -c "$(curl -fsSL https://starship.rs/install.sh)"


#compile xmonad
xmonad --recompile
